﻿using DEISW.WEB.Controllers;
using DEISW.WEB.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEISW.WEB.TestDAO
{
    [TestFixture]
    public class TestDAO
    {
        Producto producto;
        Proveedor proveedor;
        Categoria categoria;
        [Test]
        public void TestSelectUsuario()
        {
            DAO dao = new DAO();
            Usuario obj = dao.selectUsuario("1234", "1234");
            Assert.That(obj, Is.Null);
            obj = dao.selectUsuario("admin", "admin");
            Assert.That(obj.NombreUsuario, Is.EqualTo("admin"));
            Assert.That(obj.Contraseña, Is.EqualTo("admin"));
            // TODO: Add your test code here
            Assert.Pass("Your first passing test");
        }
        [Test]
        public void TestDAOAddEditDeleteProducto()
        {
            TestDAOAddEditProducto();
            TestDAODeleteProducto();
            Assert.Pass("CRUD Producto ok!");
        }
        public void TestDAOAddEditProducto()
        {
            DAO dao = new DAO();
            producto = dao.addEditProducto(null, "prodprueba", 1234, 2, 2);
            Assert.That(producto, Is.Not.Null);
            Assert.That(producto.Descripcion, Is.EqualTo("prodprueba"));
            Assert.That(producto.Monto, Is.EqualTo(1234));
            Assert.That(producto.ProveedorId, Is.EqualTo(2));
            Assert.That(producto.CategoriaId, Is.EqualTo(2));
            
        }
        public void TestDAODeleteProducto()
        {
            DAO dao = new DAO();
            dao.eliminarProducto(producto.ProductoId);
        }
        [Test]
        public void TestDAOAddEditDeleteProveedor()
        {
            TestDAOAddEditProveedor();
            TestDAODeleteProveedor();
            Assert.Pass("CRUD Proveedor ok!");
        }
        public void TestDAOAddEditProveedor()
        {
            DAO dao = new DAO();
            proveedor = dao.addEditProveedor(null, "provprueba");
            Assert.That(proveedor, Is.Not.Null);
            Assert.That(proveedor.Descripcion, Is.EqualTo("provprueba"));

        }
        public void TestDAODeleteProveedor()
        {
            DAO dao = new DAO();
            dao.eliminarProveedor(proveedor.ProveedorId);
        }
        [Test]
        public void TestDAOAddEditDeleteCategoria()
        {
            TestDAOAddEditCategoria();
            TestDAODeleteCategoria();
            Assert.Pass("CRUD Categoria ok!");
        }
        public void TestDAOAddEditCategoria()
        {
            DAO dao = new DAO();
            categoria = dao.addEditCategoria(null, "cateprueba");
            Assert.That(categoria, Is.Not.Null);
            Assert.That(categoria.Descripcion, Is.EqualTo("cateprueba"));

        }
        public void TestDAODeleteCategoria()
        {
            DAO dao = new DAO();
            dao.eliminarCategoria(categoria.CategoriaId);
        }
    }
}
