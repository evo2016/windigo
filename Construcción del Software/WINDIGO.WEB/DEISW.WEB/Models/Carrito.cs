﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DEISW.WEB.Models
{
    public class Item
    {
       public Producto producto;
       public int cantidad;
       public decimal precioTotal;
        
        public Item()
        {
            producto = new Producto();
            cantidad = 0;
            precioTotal = 0;
        }
        public void CalcularPrecioTotal()
        {
            precioTotal = cantidad * (decimal)producto.Monto;
        }
    }
    public class Carrito
    {
       public List<Item> productos;
        public Carrito()
        {
            productos = new List<Item>();
        }
        public decimal getPrecioTotal()
        {
            decimal precio=0;
            foreach (Item var in productos)
            {
                precio += var.precioTotal;
            }
            return precio;
        }
        public void AddProduct(Producto producto)
        {
            Item item=null;
            item=productos.Find(x => x.producto.ProductoId == producto.ProductoId);
            if (item != null)
            {
                int cantidadN = item.cantidad + 1;
                foreach (Item var in productos)
                {
                    if (var.producto.ProductoId == producto.ProductoId)
                    {
                        var.cantidad = cantidadN;
                        var.CalcularPrecioTotal();
                    }
                }
            }
            else
            {
                item = new Item();
                item.producto = producto;
                item.cantidad = 1;
                item.precioTotal = (decimal)producto.Monto;
                productos.Add(item);
            }
           
        }
        public void DeleteProduct(Producto producto)
        {
            Item item = productos.Find(x => x.producto.ProductoId == producto.ProductoId);
            productos.Remove(item);

        }

    }
}