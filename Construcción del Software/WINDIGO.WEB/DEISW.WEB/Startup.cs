﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DEISW.WEB.Startup))]
namespace DEISW.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
