﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DEISW.WEB.ViewModel;
using DEISW.WEB.Models;

namespace DEISW.WEB.Controllers
{
    public class Dao
    {
        DEISWEntities context;
        public Dao()
        {
            context = new DEISWEntities();
        }
        public Usuario SelectUsuario(string NombreUsuario, string Contraseña)
        {
            return context.Usuario.FirstOrDefault(x => x.NombreUsuario == NombreUsuario &&
                                                                        x.Contraseña == Contraseña);
        }
        public Categoria AddEditCategoria(int? categoriaId, string descripcion)
        {
            Categoria objCategoria = null;
            if (categoriaId.HasValue)
                objCategoria = context.Categoria.FirstOrDefault(x => x.CategoriaId == categoriaId);
            else
            {
                objCategoria = new Categoria();
                objCategoria.Estado = "ACT";
                context.Categoria.Add(objCategoria);
            }

            objCategoria.Descripcion = descripcion;
            context.SaveChanges();
            return objCategoria;
        }
        public Proveedor AddEditProveedor(int? proveedorId, string descripcion)
        {
            Proveedor objProveedor = null;
            if (proveedorId.HasValue)
                objProveedor = context.Proveedor.FirstOrDefault(x => x.ProveedorId == proveedorId);
            else
            {
                objProveedor = new Proveedor();
                objProveedor.Estado = "ACT";
                context.Proveedor.Add(objProveedor);
            }
            objProveedor.Descripcion = descripcion;
            context.SaveChanges();
            return objProveedor;
        }
        public Producto AddEditProducto(int? productoId, string descripcion, decimal? monto, int? proveedorId, int? categoriaId)
        {
            Producto objProducto = null;
            if (productoId.HasValue)
                objProducto = context.Producto.FirstOrDefault(x => x.ProductoId == productoId);
            else
            {
                objProducto = new Producto();
                objProducto.Estado = "ACT";
                context.Producto.Add(objProducto);
            }
            objProducto.Descripcion = descripcion;
            objProducto.Stock = 0;
            objProducto.Monto = (decimal)monto;
            objProducto.ProveedorId = (int)proveedorId;
            objProducto.CategoriaId = (int)categoriaId;
            context.SaveChanges();
            return objProducto;
        }
        public void EliminarProducto(int? id){
            Producto objProducto = null;
                objProducto = context.Producto.FirstOrDefault(x => x.ProductoId == id);
                objProducto.Estado = "INA";
                context.SaveChanges();
        }
        public void EliminarProveedor(int? id)
        {
            Proveedor objProveedor = null;
            objProveedor = context.Proveedor.FirstOrDefault(x => x.ProveedorId == id);
            objProveedor.Estado = "INA";
            context.SaveChanges();
        }
        public void EliminarCategoria(int? id)
        {
            Categoria objCategoria = null;
            objCategoria = context.Categoria.FirstOrDefault(x => x.CategoriaId == id);
            objCategoria.Estado = "INA";
            context.SaveChanges();
        }
    }
}