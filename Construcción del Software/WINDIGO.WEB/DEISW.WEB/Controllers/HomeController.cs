﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using DEISW.WEB.ViewModel;
using DEISW.WEB.Models;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;

namespace DEISW.WEB.Controllers
{
    public class HomeController : Controller
    {
        Boolean SessionOpen()
        {
            return Session["objUsuario"]!=null;
        }
        public ActionResult Index()
        {
            return RedirectToAction("Principal");
        }
        [HttpGet]
        public ActionResult CerrarSesion()
        {
            Session.Clear();
            return RedirectToAction("Principal");
        }

        [HttpGet]
        public ActionResult Login()
        {
            LoginViewModel objViewModel = new LoginViewModel();
            return View(objViewModel);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel objViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Dao dao = new Dao();
                    Usuario objUsuario = dao.SelectUsuario(objViewModel.NombreUsuario, objViewModel.Contraseña);
                    if (objUsuario == null)
                    {
                        objUsuario = (new DEISWEntities()).Usuario.FirstOrDefault(x => x.NombreUsuario == objViewModel.NombreUsuario);
                        if (objUsuario == null)
                        {
                            ViewBag.Mensaje = "El Usuario no se encuentra registrado en el sistema ";
                            return View(objViewModel);
                        }
                        else
                        {
                            ViewBag.Mensaje = "La contraseña ingresada es errónea";
                            return View(objViewModel);
                        }
                    }

                    Session["objUsuario"] = objUsuario;//Guardar los datos en la sesion
                    if(objUsuario.TipoUsuario=="ADM")
                        return RedirectToAction("Dashboard");
                    else
                        return RedirectToAction("Principal");
                }
                catch (Exception)
                {
                    return View(objViewModel);
                }
            }
            else
            {
                ViewBag.Mensaje = "";
                return View(objViewModel);
            }
        }
        public ActionResult Dashboard()
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            DashboardViewModel objViewModel = new DashboardViewModel();
            return View(objViewModel);
        }
        public ActionResult Principal(string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            DEISWEntities db = new DEISWEntities();
            var productos = db.Producto.Where(x => x.Estado == "ACT").ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                productos = productos.Where(s => s.Descripcion.Contains(searchString)).ToList();
            }


            int pageSize = 4;
            int pageNumber = (page ?? 1);
            return View(productos.ToPagedList(pageNumber, pageSize));

        }
        [HttpGet]
        public ActionResult LstCategoria()
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            LstCategoriaViewModel objViewModel = new LstCategoriaViewModel();
            return View(objViewModel);
        }
        [HttpPost]
        public ActionResult LstCategoria(LstCategoriaViewModel objViewModel)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            objViewModel.Fill();
            return View(objViewModel);
        }
        [HttpGet]
        public ActionResult LstProveedor()
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            LstProveedorViewModel objViewModel = new LstProveedorViewModel();
            return View(objViewModel);
        }
        [HttpPost]
        public ActionResult LstProveedor(LstProveedorViewModel objViewModel)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            objViewModel.Fill();
            return View(objViewModel);
        }
        [HttpGet]
        public ViewResult LstProducto( string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            DEISWEntities db = new DEISWEntities();
            var productos = db.Producto.Where(x=>x.Estado=="ACT").ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                productos = productos.Where(s => s.Descripcion.Contains(searchString)).ToList();
            }
           

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(productos.ToPagedList(pageNumber, pageSize));
        }
        


        [HttpGet]
        public ActionResult AddEditCategoria(int? CategoriaId)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            AddEditCategoriaViewModel objViewModel = new AddEditCategoriaViewModel();
            objViewModel.Fill(CategoriaId);
            return View(objViewModel);
        }
        
        [HttpPost]
        public ActionResult AddEditCategoria(AddEditCategoriaViewModel objViewModel)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            try
            {
                Dao dao = new Dao();
                dao.AddEditCategoria(objViewModel.CategoriaId, objViewModel.Descripcion);
                return RedirectToAction("LstCategoria");
            }
            catch(Exception)
            {
                return View(objViewModel);
            }
        }


        [HttpGet]
        public ActionResult AddEditProveedor(int? ProveedorId)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            AddEditProveedorViewModel objViewModel = new AddEditProveedorViewModel();
            objViewModel.Fill(ProveedorId);
            return View(objViewModel);
        }


        [HttpPost]
        public ActionResult AddEditProveedor(AddEditProveedorViewModel objViewModel)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            if (ModelState.IsValid)
            {
                try
                {
                    Dao dao = new Dao();
                    dao.AddEditProveedor(objViewModel.ProveedorId, objViewModel.Descripcion);
                    return RedirectToAction("LstProveedor");
                }
                catch (Exception)
                {
                    return View(objViewModel);
                }
            }
            else
            return View(objViewModel);
        }

        [HttpGet]
        public ActionResult AddEditProducto(int? ProductoId)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            AddEditProductoViewModel objViewModel = new AddEditProductoViewModel();
            objViewModel.Fill(ProductoId);
            return View(objViewModel);
        }

        public ActionResult ConfirmarProducto(string option)
        {
            ViewBag.Option = option;
            return View();
        }
        [HttpPost]
        public ActionResult AddEditProducto(AddEditProductoViewModel objViewModel)
        {
            if (!SessionOpen()) 
                return RedirectToAction("Login");
            if (ModelState.IsValid)
            {
                try
                {
                    Dao dao = new Dao();
                    dao.AddEditProducto(objViewModel.ProductoId, objViewModel.Descripcion,
                        objViewModel.Monto, objViewModel.ProveedorId, objViewModel.CategoriaId);
                    if (!objViewModel.ProductoId.HasValue)
                        return RedirectToAction("ConfirmarProducto", new { option = "C" });
                    else
                        return RedirectToAction("ConfirmarProducto", new { option = "M" });
                }
                catch (Exception)
                {
                    return View(objViewModel);
                }
            }
            else
                return View(objViewModel);
        }


        public ActionResult DeleteProducto(int? ProductoId)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            try
            {
                Dao dao = new Dao();
                dao.EliminarProducto(ProductoId);
            return RedirectToAction("LstProducto");
            }
            catch (Exception)
            {
                return RedirectToAction("LstProducto");
            }
        }

        public ActionResult DeleteCategoria(int? CategoriaId)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            try
            {
                DEISWEntities context = new DEISWEntities();
                Categoria objCategoria = null;
                objCategoria = context.Categoria.FirstOrDefault(x => x.CategoriaId == CategoriaId);
                objCategoria.Estado = "INA";
                context.SaveChanges();
                return RedirectToAction("LstCategoria");
            }
            catch (Exception)
            {
                return RedirectToAction("LstCategoria");
            }
        }

        public ActionResult DeleteProveedor(int? ProveedorId)
        {
            if (!SessionOpen()) return RedirectToAction("Login");
            try
            {
                DEISWEntities context = new DEISWEntities();
                Proveedor objProveedor = null;
                objProveedor = context.Proveedor.FirstOrDefault(x => x.ProveedorId == ProveedorId);
                objProveedor.Estado = "INA";
                context.SaveChanges();
                return RedirectToAction("LstProveedor");
            }
            catch (Exception)
            {
                return RedirectToAction("LstProveedor");
            }
        }
        [HttpGet]
        public ActionResult AddEditUsuario(int? UsuarioId)
        {
            AddEditUsuarioViewModel objViewModel = new AddEditUsuarioViewModel();
            objViewModel.Fill(UsuarioId);
            return View(objViewModel);
        }
        [HttpPost]
        public ActionResult AddEditUsuario(AddEditUsuarioViewModel objViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    DEISWEntities context = new DEISWEntities();
                    Usuario objUsuario = null;

                    if (objViewModel.UsuarioId.HasValue)
                        objUsuario = context.Usuario.FirstOrDefault(x => x.UsuarioId == objViewModel.UsuarioId);
                    else
                    {
                        objUsuario = new Usuario();
                        context.Usuario.Add(objUsuario);
                        if (context.Usuario.Where(x => x.NombreUsuario == objViewModel.NombreUsuario).FirstOrDefault() != null)
                            return View(objViewModel);
                    }

                    objUsuario.NombreUsuario = objViewModel.NombreUsuario;
                    objUsuario.Contraseña = objViewModel.Contraseña;
                    objUsuario.TipoUsuario = "CLT";
                    context.SaveChanges();

                        return RedirectToAction("Login");
                   
                }
                catch (Exception ex)
                {
                    return View(objViewModel);
                }

            }
            return View(objViewModel);

        }

        public ActionResult VerDetalle(int? ProductoId)
        {
            ProductoViewModel objViewModel = new ProductoViewModel();
            DEISWEntities context = new DEISWEntities();
            Producto objProducto = context.Producto.FirstOrDefault(x => x.ProductoId == ProductoId);
            objViewModel.Fill(objProducto);
            return View(objViewModel);
        }
       
        public ActionResult CategoriaProducto(string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            DEISWEntities db = new DEISWEntities();
            var categorias = db.Categoria.Where(x => x.Estado == "ACT").ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                categorias = categorias.Where(s => s.Descripcion.Contains(searchString)).ToList();
            }


            int pageSize = 4;
            int pageNumber = (page ?? 1);
            return View(categorias.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult VerProductosCategoria(string currentFilter, string searchString, int? page,int? CategoriaId)
        {
            if (CategoriaId.HasValue)
                Session["CategoriaId"] = CategoriaId;
            else
                 CategoriaId = (int)Session["CategoriaId"];

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            DEISWEntities db = new DEISWEntities();
            var productos = db.Producto.Where(x => x.Estado == "ACT" && x.CategoriaId==CategoriaId ).ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                productos = productos.Where(s => s.Descripcion.Contains(searchString)).ToList();
            }


            int pageSize = 4;
            int pageNumber = (page ?? 1);
            return View(productos.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult AddCarrito(int ProductoId)
        {
            Carrito carrito;
            if (Session["Carrito"] != null)
                carrito = (Carrito)Session["Carrito"];
            else
                carrito = new Carrito();
            DEISWEntities context = new DEISWEntities();
            Producto objProducto = context.Producto.FirstOrDefault(x => x.ProductoId == ProductoId);
            carrito.AddProduct(objProducto);

            Session["Carrito"] = carrito;
            return RedirectToAction("MiCarrito");
        }
        public ActionResult DeleteProductoCarrito(int ProductoId)
        {
            Carrito carrito;
            if (Session["Carrito"] == null)
                return RedirectToAction("MiCarrito");
            else
            {
                carrito = (Carrito)Session["Carrito"];
            }
            DEISWEntities context = new DEISWEntities();
            Producto objProducto = context.Producto.FirstOrDefault(x => x.ProductoId == ProductoId);
            carrito.DeleteProduct(objProducto);

            Session["Carrito"] = carrito;
            return RedirectToAction("MiCarrito");
        }
        public ActionResult MiCarrito(int? ProductoId)
        {
            Carrito carrito;
            if (Session["Carrito"] != null)
                carrito = (Carrito)Session["Carrito"];
            else
                carrito = new Carrito();
            
            return View(carrito);
        }
        public ActionResult Historial()
        {
            DEISWEntities context = new DEISWEntities();
            HistorialViewModel objViewModel = new HistorialViewModel((int?)((Usuario)(Session["objUsuario"])).UsuarioId);

            return View(objViewModel);
        }
        public ActionResult ComprarTodo()
        {
            if(Session["objUsuario"]==null || Session["Carrito"] == null)
                return RedirectToAction("MiCarrito");
            Carrito carrito;
            carrito = (Carrito)Session["Carrito"];
            DEISWEntities context = new DEISWEntities();
            Producto_ComprobantePago objProducto_Comprobante = new Producto_ComprobantePago();
            ComprobantePago objComprobantePago = new ComprobantePago();
            DateTime FechaActual = DateTime.Now;
            objComprobantePago.Fecha = FechaActual;
            objComprobantePago.UsuarioId = (int?)((Usuario)(Session["objUsuario"])).UsuarioId;
            objComprobantePago.Precio = carrito.getPrecioTotal();
            context.ComprobantePago.Add(objComprobantePago);
           context.SaveChanges();
            foreach(Item var in carrito.productos)
            {
                objProducto_Comprobante.Cantidad = var.cantidad;
                objProducto_Comprobante.ProductoId = var.producto.ProductoId;
                objProducto_Comprobante.ComprobantePagoId = objComprobantePago.ComprobantePagoId;
                context.Producto_ComprobantePago.Add(objProducto_Comprobante);
                context.SaveChanges();
            }
            Session["Carrito"] = new Carrito();
            return RedirectToAction("MiCarrito");
        }
    }
}






















