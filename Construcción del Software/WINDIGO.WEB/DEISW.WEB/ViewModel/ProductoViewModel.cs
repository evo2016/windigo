﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class ProductoViewModel
    {

        public Producto objProducto {get; set;}

        public void Fill(Producto objProducto)
        {
            this.objProducto = objProducto;
        }
    }
}