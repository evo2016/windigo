﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class LstProveedorViewModel
    {
        public List<Proveedor> LstProveedor{ get; set; }
        public String Filtro { get; set; }

        public LstProveedorViewModel()
        {
            Fill();
        }

        public void Fill()
        {
            DEISWEntities context = new DEISWEntities();
            var query = context.Proveedor.Where(x => x.Estado == "ACT").AsQueryable();

            if (!String.IsNullOrEmpty(Filtro))
                query = query.Where(x => x.Descripcion.Contains(Filtro));

            LstProveedor = query.ToList();
        }
    }
}