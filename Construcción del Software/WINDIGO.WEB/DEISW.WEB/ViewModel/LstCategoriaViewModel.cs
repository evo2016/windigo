﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class LstCategoriaViewModel
    {
        public List<Categoria> LstCategoria { get; set; }
        public String Filtro { get; set; }
        public LstCategoriaViewModel()
        {
            Fill();
        }

        public void Fill()
        {
            DEISWEntities context = new DEISWEntities();

            var query = context.Categoria.Where(x => x.Estado == "ACT").AsQueryable();

            if (!String.IsNullOrEmpty(Filtro))
            {
                query = query.Where(x =>x.Descripcion.Contains(Filtro));
            }
          
            LstCategoria = query.ToList();
        }
    }
}