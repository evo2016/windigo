﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class AddEditProveedorViewModel
    {
        public int? ProveedorId { get; set; }
        public String Descripcion { get; set; }
        public AddEditProveedorViewModel()
        {
        }

        public void Fill(int? ProveedorId)
        {
            this.ProveedorId = ProveedorId;

            if (ProveedorId.HasValue)
            {
                DEISWEntities context = new DEISWEntities();
                Proveedor objProveedor= context.Proveedor.FirstOrDefault(x => x.ProveedorId == ProveedorId);
                if (objProveedor == null) return;
                this.Descripcion = objProveedor.Descripcion;
            }
        }
    }
}