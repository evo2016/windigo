﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class LstProductoViewModel
    {
        public List<Producto> LstProducto{ get; set; }
        public String Filtro { get; set; }
        public LstProductoViewModel()
        {
            Fill();
        }

        public void Fill()
        {
            DEISWEntities context = new DEISWEntities();
            var query = context.Producto.Where(x => x.Estado == "ACT").AsQueryable();

            if (!String.IsNullOrEmpty(Filtro))
            {
                query = query.Where(x => x.Descripcion.Contains(Filtro) || x.Categoria.Descripcion.Contains(Filtro) 
                    || x.Proveedor.Descripcion.Contains(Filtro));
            }
           
            LstProducto = query.ToList();
        }
    }
}