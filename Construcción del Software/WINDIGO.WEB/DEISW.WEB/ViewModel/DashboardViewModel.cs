﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class DashboardViewModel
    {
        public Int32 NroCategoria { get; set; }
        public Int32 NroProveedor { get; set; }
        public Int32 NroProducto { get; set; }
        public DashboardViewModel()
        {
            Fill();
        }

        public void Fill()
        {
            DEISWEntities context = new DEISWEntities();
            NroCategoria = context.Categoria.Where(x => x.Estado == "ACT").Count();
            NroProveedor = context.Proveedor.Where(x => x.Estado == "ACT").Count();
            NroProducto = context.Producto.Where(x => x.Estado == "ACT").Count();
        }
    }
}