﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class AddEditCategoriaViewModel
    {
        public int? CategoriaId { get; set; }
        public String Descripcion { get; set; }

        public AddEditCategoriaViewModel()
        {
          //  CategoriaId.HasValue //TIENES VALOR TRUE O FALSE
          //  CategoriaId.Value //VALOR
        }

        public void Fill(int? CategoriaId)
        {
            this.CategoriaId=CategoriaId;
            if (CategoriaId.HasValue)
            {
                DEISWEntities context = new DEISWEntities();
                Categoria objCategoria = context.Categoria.FirstOrDefault(x => x.CategoriaId == CategoriaId);
                if (objCategoria == null) return;
                this.Descripcion = objCategoria.Descripcion;
            }
        }
    }
}