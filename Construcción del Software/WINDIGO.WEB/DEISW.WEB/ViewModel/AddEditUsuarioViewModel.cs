﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class AddEditUsuarioViewModel
    {

        public int? UsuarioId { get; set; }

        [Required(ErrorMessage = "No ha ingresado nombre")]
        public String NombreUsuario { get; set; }
        [Required(ErrorMessage = "No ha ingresado contraseña")]
        public String Contraseña { get; set; }
        

        public AddEditUsuarioViewModel()
        {
        }

        public void Fill(int? DistritoId)
        {
            this.UsuarioId = DistritoId;

            if (UsuarioId.HasValue)
            {
                DEISWEntities context = new DEISWEntities();
                Usuario objUsuario = context.Usuario.FirstOrDefault(x => x.UsuarioId == UsuarioId);
                this.NombreUsuario = objUsuario.NombreUsuario;
                this.Contraseña = objUsuario.Contraseña;
            }
        }
    }
}