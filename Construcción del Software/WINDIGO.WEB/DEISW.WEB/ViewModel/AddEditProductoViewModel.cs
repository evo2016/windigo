﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using DEISW.WEB.Models;
    
namespace DEISW.WEB.ViewModel
{
    public class AddEditProductoViewModel
    {
        public int? ProductoId { get; set; }
        [Required(ErrorMessage = "Falto llenar el campo Descripcion")]
        public String Descripcion { get; set; }
        [Required(ErrorMessage = "Falto llenar el campo Monto")]
        [Range(0, 999999999999999, ErrorMessage = " El Monto debe ser mayor a 0'")] 
        public decimal? Monto { get; set; }
        [Required(ErrorMessage = "Falto llenar el campo Categoria")]
        public int? CategoriaId { get; set; }
        [Required(ErrorMessage = "Falto llenar el campo Proveedor")]
        public int? ProveedorId { get; set; }
        public List<Categoria> ListaCategoria { get; set; }
        public List<Proveedor> ListaProveedor { get; set; }


        public AddEditProductoViewModel()
        {
            DEISWEntities context = new DEISWEntities();
            ListaCategoria = context.Categoria.Where(x => x.Estado != "INA").ToList();
            ListaProveedor = context.Proveedor.Where(x => x.Estado != "INA").ToList();
        }

        public void Fill(int? ProductoId)
        {
            this.ProductoId = ProductoId;

            if (ProductoId.HasValue)
            {
                DEISWEntities context = new DEISWEntities();
                Producto objProducto = context.Producto.FirstOrDefault(x => x.ProductoId == ProductoId);
                if (objProducto == null) return;
                this.Descripcion=objProducto.Descripcion;
                this.Monto=objProducto.Monto;
                this.ProveedorId = objProducto.ProveedorId;
                this.CategoriaId = objProducto.CategoriaId;

            }
        }
    }
}
