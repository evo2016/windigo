﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DEISW.WEB.Models;

namespace DEISW.WEB.ViewModel
{
    public class HistorialViewModel
    {
        public int? UsuarioId { get; set; }
        
        public List<ComprobantePago> ListaComprobante { get; set; }


        public HistorialViewModel(int? UsuarioId)
        {
            DEISWEntities context = new DEISWEntities();
            ListaComprobante = context.ComprobantePago.Where(x => x.UsuarioId == UsuarioId).ToList();
        }
    }
}
